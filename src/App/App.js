import './App.css';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from '../Components/Core/Header';
import ApiDetails from '../Components/ApiState/ApiDetail';
import Apilist from '../Components/ApiState/ApiList';
import Register from '../Components/Formulario';
import styled from '@emotion/styled';
import MainComponent from '../Components/Home';
import Cart from '../Components/Cart/Cart';
import { useState } from 'react';


const Bodystyled = styled ('body')({
  background: '#E1E1E1'
})

function App() {
  const [cartItems, setCartItems] = useState([]);
  const onAdd = (item) => {
    const exist = cartItems.find((x) => x.id === item.id);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x.id === item.id ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...item, qty: 1 }]);
    }
  };

  const onRemove = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x.id !== product.id));
    } else {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
        )
      );
    }
  };


  return (
    <Router>
      <Bodystyled>
      <Header />
      
      <Switch>

      <Route path="/lista/:id">
         <ApiDetails /> 
        </Route>
      
        <Route path="/formulario">
          <Register />
        </Route>

        <Route path="/cart" exact>
          Cart
        </Route>

        <Route path="/lista">
         <Apilist  onAdd={onAdd}></Apilist> 
         <Cart cartItems={cartItems}
          onAdd={onAdd}
          onRemove={onRemove}
        ></Cart>
        </Route>

        

        <Route path="/" exact>
          <MainComponent />
        </Route>
        
        
      </Switch>

      </Bodystyled>
      
      

      
      
    

    </Router>
    
  );
}

export default App;
