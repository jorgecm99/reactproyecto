import React from 'react';
import { useParams } from 'react-router-dom';
import styled from '@emotion/styled';

const ApiDetails = () => {

    const {id} = useParams()

    const [ detalle, setDetalle] = React.useState([])

    React.useEffect(() => {
        const obtenerDatos = async () => {
            const data = await fetch(`https://fakestoreapi.com/products/${id}`)
            const orders = await data.json()
            setDetalle(orders)
        }
        obtenerDatos()
    },[id])

    const Button = styled ('button') ({
        border: 'none',
        background: 'darkblue',
        color: 'white',
        fontWeight: 'bold',
        padding: '8px',
        borderRadius: '3px'
    })
    


    return (
        <section className="detalle">
            <div>
            <h3>{detalle.title}</h3> - <h3>{detalle.category}</h3>
            <h3>{detalle.description}</h3>
            <img className="image-detalle" src={detalle.image} alt={detalle.title} ></img>
        </div>

        <div>
            <h3>{detalle.price}$</h3>
            <Button>Add to cart</Button>
            
        </div>

        </section>
        
    )

}


export default ApiDetails