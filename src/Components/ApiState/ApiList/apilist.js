import React from 'react';
import { Link } from 'react-router-dom';
import styled from '@emotion/styled';



const Imagencont = styled ('img')({
    width: '40%',
})

const Price = styled ('div') ({
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '15px',
    marginLeft: '15px',
    alignItems: 'center',
    position: 'absolute',
    bottom: '15px',
    left: '15px',
    right: '15px'

})

const Button = styled ('button') ({
    border: 'none',
    background: 'darkblue',
    color: 'white',
    fontWeight: 'bold',
    padding: '8px',
    borderRadius: '3px',
})

const PriceLetter = styled ('p') ({
    fontWeight: 'bold'
})

export default function Apilist (props) {

    const [ datos, setDatos] = React.useState([]);
    

    React.useEffect(() => {
        obtenerDatos()
    },[])

    const obtenerDatos = async () => {
        const data = await fetch('https://fakestoreapi.com/products')
        const orders = await data.json()
        setDatos(orders)
    }

    const {item, onAdd} = props;


    return (
        <div className="container-principal">
       
            {
                datos.map(item => (
                    
                        <div className="principal" key={JSON.stringify(item)}>
            
                            <Link to={`/lista/${item.id}`} style={{textDecoration:"none"}} >
                            <Imagencont src={item.image} alt={item.title} />
                                <h4>{item.title}</h4>        
                            </Link>
                            <Price>
                                <PriceLetter>{item.price}$</PriceLetter>
                                <Button onClick={() => onAdd(item)}>Add to cart</Button>
                            </Price> 
            
            
                        </div>

                    
                    
              ))}
                    
                
            
        
        </div>
        
    )
};

