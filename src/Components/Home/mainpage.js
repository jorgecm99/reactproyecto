import React from 'react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';

const Mainimage = styled ('div')({
    width: '100%',
    margin: '0',
    position:'relative'
})

const Mainpicture = styled ('img')({
    maxWidth: '100%',

    
})

const Buttonstyle = styled ('button')({
    border: '2.5px solid white',
    borderRadius: '3px',
    background: 'white',
    opacity: '0.65',
    color: 'white',
    fontWeight: 'bold',
    width: '200px',
    position: 'absolute',
    left: '15%',
    top:'75%'

})

const MainComponent = () => {
    return(
        <Mainimage>
            <Mainpicture src="https://www.aboutmanchester.co.uk/wp-content/uploads/2020/09/Shop.jpg" alt="outlet"></Mainpicture>
            <Buttonstyle>
                <Link to="/lista">
                    SHOP
                </Link>
            </Buttonstyle> 
            
        </Mainimage>
    )

}

export default MainComponent