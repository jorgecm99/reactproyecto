import React from 'react';
import { Link } from 'react-router-dom';
import styled from '@emotion/styled';



const HeaderStyle = styled ('header')({
    width: '100%',
    height: '80px',
    background: 'blue',
    boxShadow: '0 2px',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
})

const NavHeader = styled ('nav')({
    margin: '0'
})


const Navstyle = styled ('ul')({
    display: 'flex',
    listStyle: 'none',
    marginBottom: '0',
    marginRight: '50px'
    

})

const LinkStyle = styled ('li')({
    marginLeft: '10px',
    listStyle: 'none',
    fontFamily: 'Verdana',
    textTransform: 'uppercase',
    cursor: 'pointer',
    fontWeight: 'bold',
    fontSize: '20px',
    padding: '10px',
    ":hover":{
        background: 'lightblue',
        
    }   
})


const LinkStyle2 = styled ('li')({
    marginLeft: '50px',
    listStyle: 'none',
    fontFamily: 'Verdana',
    textTransform: 'uppercase',
    cursor: 'pointer',
    fontWeight: 'bold',
    fontSize: '20px',
    padding: '10px',
    ":hover":{
        background: 'orange',
        
    }   
})


const LinkStyleForm = styled ('li')({
    marginLeft: '10px',
    listStyle: 'none',
    fontFamily: 'Verdana',
    textTransform: 'uppercase',
    cursor: 'pointer',
    fontWeight: 'bold',
    fontSize: '20px',
    padding: '10px',
    ":hover":{
        background: 'red',
    }
    
   
})

const Header = (props) => {
    return (
        
            <HeaderStyle>
                
                
                    <LinkStyle2>
                    <Link to="/" style={{textDecoration:"none", color: '#F7F3F3'}}>Home</Link>
                    </LinkStyle2>

                    <NavHeader>
                    <Navstyle>

                    <LinkStyle>
                    <Link to="/lista" style={{textDecoration:"none", color: '#F7F3F3'}}>
                        Products
                    </Link>
                    </LinkStyle>

                    <LinkStyleForm>
                    <Link to="/formulario" style={{textDecoration:"none", color: '#F7F3F3'}}>
                        Sign Up
                    </Link></LinkStyleForm>

                    
                    <LinkStyle>
                    <Link to="/cart">
                        Cart{' '}
                            {props.countCartItems ? (
                                <button className="badge">{props.countCartItems}</button>
                                    ) : (
                                    ''
                            )}
                    </Link>
                    </LinkStyle>
                    </Navstyle>
                    </NavHeader>

                    
                

            </HeaderStyle>

            
           
        

        
        
    )

};


export default Header