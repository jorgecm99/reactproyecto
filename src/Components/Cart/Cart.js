import React from 'react';
import styled from '@emotion/styled';

export default function Cart (props) {
    const { cartItems = [],   
            onAdd, 
            onRemove } = props;

    const itemsPrice = cartItems.reduce((a, c) => a + c.qty * c.price, 0);
    const taxPrice = itemsPrice * 0.14;
    const shippingPrice = itemsPrice > 2000 ? 0 : 20;
    const totalPrice = itemsPrice + taxPrice + shippingPrice;
    
    const Button = styled ('button') ({
      width: '40px',
      height: '40px',
      border: 'none',
      background: 'orange',
      color: 'white',
      fontSize: '16px',
      fontWeight: 'bold',
      padding: '8px',
      borderRadius: '3px'
  })

    return (
      <aside className="aside">
      <h2>Cart Items</h2>
      <div>
        {cartItems?.length === 0 && <h4>Your cart is empty</h4>}
        {cartItems.map((item) => (
          <div key={item.id} className="row">
            <div className="col-2">{item.name}</div>
            <div className="col-2">
              <Button onClick={() => onRemove(item)} className="remove">
                -
              </Button>{' '}
              <Button onClick={() => onAdd(item)} className="add">
                +
              </Button>
            </div>

            <div className="col-4 text-right">
              {item.qty} x ${item.price.toFixed(2)}
            </div>
          </div>
        ))}

{cartItems.length !== 0 && (
          <>
            <hr></hr>
            <div className="row">
              <div className="col-2">Items Price</div>
              <div className="col-1 text-right">${itemsPrice.toFixed(2)}</div>
            </div>
            <div className="row">
              <div className="col-2">Tax Price</div>
              <div className="col-1 text-right">${taxPrice.toFixed(2)}</div>
            </div>
            <div className="row">
              <div className="col-2">Shipping Price</div>
              <div className="col-1 text-right">
                ${shippingPrice.toFixed(2)}
              </div>
            </div>

            <div className="row">
              <div className="col-2">
                <strong>Total Price</strong>
              </div>
              <div className="col-1 text-right">
                <strong>${totalPrice.toFixed(2)}</strong>
              </div>
            </div>
            <hr />
            <div className="row">
              <button onClick={() => alert('Your total is !')}>
                Checkout
              </button>
            </div>
          </>
        )}

       
      </div>
    </aside>

    )
}